Create 2 services in Express
1. Service Name : uploadData
    - Data Format : [{"name":"abc","gender":"male","age":"22"},{},{}]
    - File Name to store data : data_<DATE>_<RANDOMNUMBER>.csv (data_14012020_100.csv , should not get duplicated)
    - Validate Data properly.

2. Service Name : sort/:gender
    - Take data from file uploaded by uploadData service
    - File Name to store data : data_<DATE>_<RANDOMNUMBER>.csv (data_14012020_100.csv , should not get duplicated)
    - Validate Data properly.

CSV File Format
name,gender,age
abc,MALE,20
xyz,MALE,3   