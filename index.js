const express=require("express");
const app=express();
const fs = require('fs');
const path = require('path');
const os = require('os');
const moment = require('moment');
const bodyParser=require("body-parser");

//Middleware
//app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.post("/uploadData",  function(req,res) {
	object=req.body;	

	// ---- Validation -----
	out_str=[];

	for(x of object){
		var letters = /^[A-Za-z]+$/;
		if((x.name)=="" || (x.name)==null || (x.gender)=="" || (x.gender)==null || (x.age)=="" || (x.age)==null){
			if(!out_str.includes("Fields should not be empty or null <br />")){
				out_str.push("Fields should not be empty or null <br />");
			}
		}
		if(!/^[a-zA-Z]*$/g.test(x.name) || !/^[a-zA-Z]*$/g.test(x.gender)){
			if(!out_str.includes("Name and gender fields should include characters only <br />")){
				out_str.push("Name and gender fields should include characters only <br />");
			}
		}
		if(isNaN(x.age)){
			if(!out_str.includes("Age fields should include digits only <br />")){
				out_str.push("Age fields should include digits only <br />");
			}
		}
	}

	// ---- Creating CSV file ----
	if (out_str.length == 0){ // ---- After successfull validation ----
		// get date
		var date = moment().format('DD,MM,YYYY');
		date=(date.split(",")).join("");
		// checking existence of file name
		var fname="data_"+date+"_"+(Math.floor(Math.random() * 1000) + 100);
		fs.stat('foo.txt', function(err, stat) {
			if(err == null) {
				console.log('File exists');
			} 
			else if(err.code === 'ENOENT') {
				console.log('File does not exists');
				// output file in the same folder
				const filename = path.join(__dirname, `${fname}.csv`);
				const output = []; // holds all rows of data

				output.push(["name","gender","age"])
				object.forEach((d) => {
					const row = []; // a new array for each row of data
					row.push(d.name);
					row.push(d.gender);
					row.push(d.age);
					output.push(row.join()); // by default, join() uses a ','
				});

				fs.writeFileSync(filename, output.join(os.EOL));
				console.log('A new file is created ! ');
				res.send(`A file is created with name : ${fname}.csv`);
			} 
			else {
				console.log('Some other error: ', err.code);
			}
		});
	}
	else{ // ---- After un-successfull validation ----
		out_str_new = "Caution : <br />";
		num=0;
		for (x of out_str){
			out_str_new += (++num)+" : "+x;
			console.log(x);
		}
		res.send(out_str_new);
	}
})


app.get("/sort/:gender",  function(req,res) {
	fs.readFile('./data_12022020_783.csv', 'utf8', function (err, data) { // File reading
		if(err){
			res.send("File is not exists ! ");
		}
		else{
			var tmpArray=data.split(/\r?\n/);
			var dataArray=[];
			for (i=1; i < tmpArray.length ;i++){
				dataArray.push(tmpArray[i].split(","));		
			}
			dataArray.sort();

			gender_value=(req.params.gender).toLowerCase();
			console.log(gender_value+" "+typeof gender_value);
			tmp_str=["name,gender,age"];
			
			if(gender_value == "male" || gender_value == "m"){
				for ( x of dataArray){
					if((x[1]).toLowerCase() == "male" || ((x[1]).toLowerCase()) == "m"){
						tmp_str.push(x[0]+','+x[1]+','+x[2]);
					}
				}
			}
			else if(gender_value == "female" || gender_value == "f"){
				for ( x of dataArray){
					if((x[1]).toLowerCase() == "female" || ((x[1]).toLowerCase()) == "f"){
						tmp_str.push(x[0]+','+x[1]+','+x[2]);
					}
				}
			}
			out_str="";
			for(x of tmp_str){
				out_str += x+"<br />";
			}	
			res.send(out_str);
		}
	});
})

app.listen(1005, function(){
	console.log("Server is runnning on port 1005")
})